#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <fstream>
#include <stack>
using namespace std;


int no_of_component;
bool Sort(const pair<int,int> &a,const pair<int,int> &b){
	return (a.first < b.first);
}

class Edge {
public:
    int u,v;
    Edge(int u, int v){
    	this->u = u;
    	this->v = v;
    }
};
void getBCC(int, vector<int>&, vector<int>&, stack<Edge>&, vector<int>&, vector<int>&, vector<int>&);


int main(int argc, char** argv){
	if(argc != 2){
		cout << "Input error." << endl;
	}else{
		int no_of_edges, no_of_vertices, total_edges;
		ifstream inputFile(argv[1]);
		inputFile >> no_of_vertices >> no_of_edges;
		total_edges = 2 * no_of_edges;

		int u, v;


		vector<pair <int,int> > edges;

		for(int i = 0; i < no_of_edges; ++i){
			inputFile >> u >> v;
			u = u - 1;
			v = v - 1;
			edges.push_back(make_pair(u,v));
			edges.push_back(make_pair(v,u));
		}
		sort(edges.begin(), edges.end(), Sort);


		vector<int> pointer (no_of_vertices+1);
		vector<int> arcs(total_edges);
		int prev = -1;
		int j = 0;
		for(int i = 0; i < total_edges; i++){
			arcs[i] = edges[i].second;
			if(prev != edges[i].first){
				pointer[j++] = i;
				prev = edges[i].first;
			}
		}
		pointer[j] = total_edges;


		edges.clear();



		vector<int> disc(no_of_vertices, -1);
		vector<int> low(no_of_vertices, -1);
		vector<int> parent(no_of_vertices, -1);
		stack<Edge> st;

		for(int i = 0; i < no_of_vertices; ++i){
			if(disc[i] == -1){
				getBCC(i, disc, low, st, parent, pointer, arcs);
			}
			int flag = 0;

			
			if(st.size() > 0){
				cout << "biconnected component "<< ++no_of_component << " :" << "\n";
			}
			while(st.size() > 0){
				cout << "( " << st.top().v + 1 << ", " << st.top().u + 1<< "),  ";
				st.pop();
			}
		}
	}
}


void getBCC(int u,vector<int>& disc, vector<int>& low, stack<Edge>& st,  vector<int>& parent, vector<int>& pointer, vector<int>& arcs){

    static int time = 0;

    disc[u] = ++time;
    low[u] = disc[u];

    for (int i = pointer[u]; i != pointer[u+1]; ++i) {
        int v = arcs[i]; 

        if (disc[v] == -1) {
            parent[v] = u;

            st.push(Edge(u, v));
            getBCC(v, disc, low, st, parent, pointer, arcs);

            low[u] = min(low[u], low[v]);
            

            if ( (disc[u] == 1) || (disc[u] > 1 && low[v] >= disc[u])) {
				cout << "Biconnected Component "<< ++no_of_component << " :" << "\n{ ";
                while (st.top().u != u || st.top().v != v) {
                    cout << "( " << st.top().u + 1 << ", " << st.top().v + 1<< "),  ";
                    st.pop();
                }
				cout << "( " << st.top().u + 1 << ", " << st.top().v + 1<< ") }\n\n";
                st.pop();
            }
        }


        else if (v != parent[u]) {
            low[u] = min(low[u], disc[v]);
            if (disc[v] < disc[u]) {
                st.push(Edge(u, v));
            }
        }
    }
}
