import React, {useState}from 'react'
import { Form, Button } from "react-bootstrap";

const Add_Student = ({addStudent}) =>  {
  const [name, setname] = useState('');
  const [course, setcourse] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(name);
    console.log(course);
    // console.log(addStud);
    const a = {name,course};
    addStudent(a);
    setname("");
    setcourse("");
  }

  return (
    <div className="container">
      <center><h2>Add New Student</h2></center>

      <Form onSubmit={handleSubmit} >
        <Form.Group>
          <Form.Label><strong>Student Name</strong></Form.Label>
          <Form.Control type="text" placeholder="Enter Name" required onChange = {(e) => setname(e.target.value)} />
        </Form.Group>

        <Form.Group>
          <Form.Label><strong>Course</strong></Form.Label>
          <Form.Control type="text" placeholder="Enter Course Name" required onChange = {(e) => setcourse(e.target.value)} />
        </Form.Group>
        <Button variant="primary" type="submit">
          Add Student
        </Button>
      </Form>
    </div>
  );
}
export default Add_Student;