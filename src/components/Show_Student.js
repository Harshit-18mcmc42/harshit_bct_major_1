import React, {useState}from 'react'

import {Table,Button,Form} from "react-bootstrap";

const Show_Student = ({ student, addAttendence }) => {
  // console.log(student);

  const [roll, setroll] = useState(0);


  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log(addStud);
    // const a = {name,course};
    // const r = parseInt(roll);
    const hour = 1;
    console.log(roll);
    console.log(hour);
    const a = {roll,hour};
    addAttendence(a);
  }

  return (
    <>
      <div className = "container">
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Course</th>
            <th>Attendence in Hour</th>
            <th>Add Attendence</th>
          </tr>
        </thead>
        <tbody>
          {student.map((eachStudent) => (
            <tr key = {eachStudent.roll_No}>
              <td>{eachStudent.roll_No}</td>
              <td>{eachStudent.name}</td>
              <td>{eachStudent.course}</td>
              <td>{eachStudent.attendence_In_Hour}</td>
              <td> <Form onSubmit = {handleSubmit}><Button variant="primary" type="submit" onClick = {(e) => setroll(eachStudent.roll_No)}  >Add Attendence</Button></Form></td>
            </tr>
          ))}
        </tbody>
      </Table>
      </div>
    </>
  );
};
export default Show_Student;