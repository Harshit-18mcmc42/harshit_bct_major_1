import React, {useState}from 'react'
import {Table,Form,Button} from "react-bootstrap";

const ShowAdmin = ({ admin , deleteAdmin}) => {

  const [Index, setIndex] = useState(0);


  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log(addStud);
    // const a = {name,course};
    // const r = parseInt(roll);
    const a = {Index};
    deleteAdmin(a);
  }


  // console.log(student);
  return (
    < >
      <div className = "container">
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>Address</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {admin.map((eachAdmin,index) => (
            <tr key = {index}>
              <td>{index}</td>
              <td>{eachAdmin}</td>
              {index === 0 ? <td><strong>Super - 42</strong></td> : <td> <Form onSubmit = {handleSubmit}><Button variant="danger" type="submit" onClick = {(e) => setIndex(eachAdmin)}  >Delete</Button></Form></td>}
            </tr>
          ))}
        </tbody>
      </Table>
      </div>
    </>
  );
};
export default ShowAdmin;
