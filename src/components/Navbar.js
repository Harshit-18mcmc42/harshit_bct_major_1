import React from "react";
import logo from "../images/logo.png"
const Navbar = ({ account }) => {
  const sortAccount = account.slice(0, 7);
  return (
    <nav className="navbar navbar-dark bg-dark shadow mb-5">
      <p className="navbar-brand my-auto"><img src = {logo} alt = "mylogo" className = "myimage"/></p>
      <ul className="navbar-nav">
        <li className="nav-item text-white text-primary"><strong>Hello.. {sortAccount}</strong></li>
      </ul>
    </nav>
  );
};

export default Navbar;
