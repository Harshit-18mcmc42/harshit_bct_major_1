import React, {useState}from 'react'
import { Form, Button } from "react-bootstrap";

const Add_Admin = ({ addAdmin }) => {
  const [address, setaddress] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(address);
    const a = { address };
    addAdmin(a);
    setaddress("");
  };

  return (
    <div className="container">
      <center><h2>Add New Admin</h2></center>
      <Form onSubmit={handleSubmit} className = "myform">
        <Form.Group>
          <Form.Label><strong>New Address</strong></Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter address"
            required
            onChange={(e) => setaddress(e.target.value)}
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Add Admin
        </Button>
      </Form>
    </div>
  );
};
export default Add_Admin;
