import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Loader from "../images/loader.svg";

import Harshit_Contract_abi from "../contracts/Harshit_Contract.json";
import Web3 from "web3";

/* Components */
import Navbar from "../components/Navbar";
import AddStudent from "../components/Add_Student";
import ShowStudent from "../components/Show_Student";
import AddAdmin from "../components/Add_Admin";
import ShowAdmin from "../components/ShowAdmin";

toast.configure();
function Harshit() {
  const [loading, setLoading] = useState(true);
  const [loading2, setloading2] = useState(false);
  const [account, setAccount] = useState("");
  const [refresh, setrefresh] = useState(0);
  const [students, setstudents] = useState({});
  const [admins, setadmins] = useState({});
  const [Harshit, setHarshit] = useState({});

  /* Web3 Connection */

  const loadWeb3 = async () => {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum);
      await window.ethereum.enable();
    } else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider);
    } else {
      window.alert(
        "Non-Ethereum browser detected. You should consider trying MetaMask!"
      );
      toast.error("Non-Ethereum browser ..", { position: toast.POSITION.TOP_CENTER });

    }
  };

  const loadBlockchainData = async () => {
    setLoading(true);
    if (
      typeof window.ethereum == "undefined" ||
      typeof window.web3 == "undefined"
    ) {
      return;
    }
    const web3 = window.web3;

    const accounts = await web3.eth.getAccounts();

    if (accounts.length === 0) {
      return;
    }
    setAccount(accounts[0]);
    const networkId = await web3.eth.net.getId();

    const networkData = Harshit_Contract_abi.networks[networkId];
    console.log(networkData);
    if (networkData) {
      const Harshit_42 = new web3.eth.Contract(
        Harshit_Contract_abi.abi,
        networkData.address
      );
      setHarshit(Harshit_42);
      const s = await Harshit_42.methods.showStudents().call();
      setstudents(s);
      const a = await Harshit_42.methods.showAdmin().call();
      setadmins(a);
      setLoading(false);
    } else {
      // window.alert("the contract not deployed to detected network.");
      toast.info("The contract not deployed to detected network.. Please switch to Ropsten Test Network..", { position: toast.POSITION.TOP_CENTER ,autoClose: 8000});

      setloading2(true);
    }
  };

  const addStudent = async ({ name, course }) => {
    var flag = 0;
    setLoading(true);

    try{
    await Harshit.methods
      .addStudent(name, course)
      .send({ from: account })
      .on("transactionHash", () => {
        toast.success("Trying to add student.", { position: toast.POSITION.TOP_CENTER, autoClose: 15000 });
      });
    }catch(err){
      flag = 1;
      toast.error("Error : May be u r not Admin", { position: toast.POSITION.TOP_CENTER });
    }
    if(flag === 0){
      toast.success("Student Added Successfully", { position: toast.POSITION.TOP_CENTER, autoClose: 15000 });
    }
    setLoading(false);
    loadBlockchainData();
  };

  const addAdmin = async ({ address }) => {
    var flag = 0;
    setLoading(true);
    try {
      await Harshit.methods
        .addAdmin(address)
        .send({ from: account })
        .on("transactionHash", () => {
          toast.success("Trying to add admin...", { position: toast.POSITION.TOP_CENTER , autoClose: 15000});
        });
    } catch (err) {
      flag = 1;
      toast.error("Error : May be u r not Admin or Check address..", { position: toast.POSITION.TOP_CENTER , autoClose: 15000});
    }
    if(flag === 0){
      toast.success("Admin Added successfully..", { position: toast.POSITION.TOP_CENTER , autoClose: 15000});
    }
    setLoading(false);
    loadBlockchainData();
  };

  const addAttendence = async ({ roll, hour }) => {
    var flag = 0;
    setLoading(true);
    var a = parseInt(roll);
    var b = parseInt(hour);
    try{
    await Harshit.methods
      .increaseAttendence(a, b)
      .send({ from: account })
      .on("transactionHash", () => {
        toast.success("Trying to Add attendence ....", { position: toast.POSITION.TOP_CENTER, autoClose: 15000 });
      });
    }catch(err){
      flag = 1;
      toast.error("Error : May be u r not Admin ..", { position: toast.POSITION.TOP_CENTER });
    }
    if(flag === 0){
      toast.success("Attendence Added successfully..", { position: toast.POSITION.TOP_CENTER, autoClose: 15000 });
    }
    setLoading(false);
    loadBlockchainData();
  };

  const deleteAdmin = async ({ Index }) => {
    var flag = 0;
    setLoading(true);
    try{
    await Harshit.methods
      .removeAdmin(Index)
      .send({ from: account })
      .on("transactionHash", () => {
        toast.success("Trying to delete admin...", { position: toast.POSITION.TOP_CENTER , autoClose: 15000});
      });
    }catch(err){
      flag = 1;
      toast.error("Error : You R not Super-Admin", { position: toast.POSITION.TOP_CENTER });
    }
    if(flag === 0){
      toast.success("Admin Deleted successfully..", { position: toast.POSITION.TOP_CENTER , autoClose: 15000});
    }
    setLoading(false);
    loadBlockchainData();
  };

  useEffect(() => {
    loadWeb3();
    loadBlockchainData();
    if (refresh === 1) {
      setrefresh(0);
      loadBlockchainData();
    }
  }, [refresh]);

  return loading === true ? (
    <p className="text-center">
      <div className = "loader"> <img src={Loader} alt="loding.io svg" /></div>{loading2 ? <div className = "loader"> <img src={Loader} alt="loding.io svg" /></div> : ""}
    </p>
  ) : (
    <>
      <Navbar account={account} />
      <div className="Container mr-3 ml-3">
        <div className="row justify-content-center">
          <div className="col-lg-6 col-md-10">
            <ShowStudent student={students} addAttendence={addAttendence} />
          </div>
          <div className="col-lg-6 col-md-10">
            <AddStudent addStudent={addStudent} />
          </div>
        </div>
        <div className="row justify-content-center mt-3">
          <div className="col-lg-6 col-md-10 ">
            <ShowAdmin admin={admins} deleteAdmin={deleteAdmin} />
          </div>
          <div className="col-lg-6 col-md-10">
            <AddAdmin addAdmin={addAdmin} />
          </div>
        </div>
      </div>
    </>
  );
}
export default Harshit;
