// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.8.0;
pragma experimental ABIEncoderV2;

contract Harshit_Contract {
    
    
    
    
    // ******** VARIABLES AND DECLARATION ********
    
    // Array for Storing Admin's address.
    address[] public Admin_List;
    
    // Structure for Studnet
    struct Student {
        uint roll_No;
        string name;
        string course;
        uint attendence_In_Hour;
    }
    
    event student_Updation (
        uint roll_No,
        string name,
        string course,
        uint attendence_In_Hour
    );
    
    // Global Varibale for auto increment Roll_Number.
    uint public next_Roll_No = 0;
    
    // Array of Student for Storing studnets detials.
    Student[] public students;
    
    
    
    
    
    // ********** UTILITY FUNCTIONS **********
    
    // 1)  For checking current user is admin or not.
    function checkAdmin() internal view returns (bool) {
        for(uint i = 0; i < Admin_List.length; i++){
            if(Admin_List[i] == msg.sender){
                return true;
            }
        }
        return false;
    }
    
    // 2)  Creating modifier for Admin_Only
    function onlyAdmin() public view{
        require(checkAdmin() == true, "Sorry You are not Admin");
    }
    
    // 3)  Function for Validationg address
    function validAddress(address _addr) public pure {
        require(_addr != address(0), "Not a Valid Address ");
    }
    
    // 4) For checking SuperAdmin.              
    function onlySuperAdmin() public view {                                 
        require(Admin_List[0] == msg.sender, "Sorry.. Your are not SuperAdmin.");
    }
    



    // **************** CONSTRUCTOR AND REQUIRED FUNCTIONS **************
    
    // Address of SuperAdmin is stored at index 0 of Admin_List at the time of deployment. 
    constructor() {
        Admin_List.push(msg.sender);
    }
    
    // Function for adding Admin address to the Admin_List.
    function addAdmin(address addr) public {
        onlyAdmin();
        validAddress(addr);
        Admin_List.push(addr);
    }
    
    // Function for Deleting Admin from Admin_List
    function removeAdmin(address addr) public returns (address) {
        onlySuperAdmin();
        validAddress(addr);
        for(uint i = 0; i < Admin_List.length; i++){
            if(Admin_List[i] == addr){
                delete Admin_List[i];
                Admin_List.pop();
                return addr;
            }
        }
        revert("User dosen't exist");
    }

    // Function to Add Student
    function addStudent(string memory _name, string memory _course) public{
        onlyAdmin();
        uint _attendence_In_Hour = 0;
        next_Roll_No++;
        students.push(Student(next_Roll_No, _name, _course, _attendence_In_Hour));
        emit student_Updation(next_Roll_No, _name, _course, _attendence_In_Hour);
    }
    
    // Function to increase Attendence hour.
    function increaseAttendence(uint _roll_No, uint _attendence_In_Hour) public returns (uint){
        onlyAdmin();
        for(uint i = 0; i < students.length; i++){
            if(students[i].roll_No == _roll_No){
                students[i].attendence_In_Hour += _attendence_In_Hour;
                return students[i].attendence_In_Hour;
            }
        }
        revert("Studnet is not present ..");
    }
    
    // Function to Show List of Students.
    function showStudents() public view returns (Student[] memory) {
        return students;
    }
    
    // Function to Show list of Admin.
    function showAdmin() public view returns (address[] memory) {
        return Admin_List;
    }

}